// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSProjectGameMode.h"
#include "TDSProjectPlayerController.h"
#include "TDSProject/Character/TDSProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDSProjectGameMode::ATDSProjectGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDSProjectPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}